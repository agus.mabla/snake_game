class Snake{
    constructor(filas, cols){
        this.filas_tablero = filas;
        this.columnas_tablero = cols;
        this.snake = Array(Array(Math.round(Math.random()*(filas-1)+1), Math.round(Math.random()*cols)));
        this.direccion = 0; //Direccion: 0 -> Arriba || 1 -> Derecha || 2 -> Abajo || 3 -> Izquierda
    }
    
    arriba(){
        this.direccion = 0;
    }
    abajo(){
        this.direccion = 2;
    }
    izquierda(){
        this.direccion = 3;
    }
    derecha(){
        this.direccion = 1;
    }
    puede_avanzar(){ //Devuelve false en caso de game over, true si puede moverse. 
        let libre = true;
        switch(this.direccion){
            case 0:
                if(this.snake[0][0] == 0){
                    return 0;
                }
                this.snake.forEach(posicion =>{
                    if(posicion[0] == this.snake[0][0] -1 && posicion[1] == this.snake[0][1]){
                        libre = false;
                    }
                });
                if((!libre) && ((this.snake[0][0]-1 != this.snake[this.snake.length -1][0]) || (this.snake[0][1] != this.snake[this.snake.length -1][1]))){
                    return false;
                }
                break;

            case 1:
                if(this.snake[0][1] == this.columnas_tablero-1){
                    return 0;
                }
                this.snake.forEach(posicion =>{
                    if(posicion[0] == this.snake[0][0] && posicion[1] == this.snake[0][1] +1){
                        libre = false;
                    }
                });
                if((!libre) && ((this.snake[0][0] != this.snake[this.snake.length -1][0]) || (this.snake[0][1]+1 != this.snake[this.snake.length -1][1]))){
                    return false;
                }
                break;

            case 2:
                if(this.snake[0][0] == this.filas_tablero-1){
                    return false;
                }
                this.snake.forEach(posicion =>{
                    if(posicion[0] == this.snake[0][0] +1 && posicion[1] == this.snake[0][1]){
                        libre = false;
                    }
                });
                if((!libre) && ((this.snake[0][0]+1 != this.snake[this.snake.length -1][0]) || (this.snake[0][1] != this.snake[this.snake.length -1][1]))){
                    return 0;
                }
                break;
            case 3:
                if(this.snake[0][1] == 0){
                    return false;
                }
                this.snake.forEach(posicion =>{
                    if(posicion[0] == this.snake[0][0] && posicion[1] == this.snake[0][1] -1){
                        libre = false;
                    }
                });
                if((!libre) && ((this.snake[0][0] != this.snake[this.snake.length -1][0]) || (this.snake[0][1]-1 != this.snake[this.snake.length -1][1]))){
                    return false;
                }
                break;
        }
        return true;
    }
    avanzar(manzana){//Devuelve 1 en caso que avanze, 0 en caso que no pueda, 3 en caso que se haya comido la manzana.
        if(this.puede_avanzar()){
            let crecer = false;
            let tmp = [];
            switch(this.direccion){
                case 0:
                    if(this.snake[0][0]-1==manzana.posicion[0]&& this.snake[0][1]==manzana.posicion[1]){
                        tmp = this.snake.slice(-1);
                        crecer = true;
                    }
                    break;
                case 1:
                    if(this.snake[0][0]==manzana.posicion[0]&& this.snake[0][1]+1==manzana.posicion[1]){
                        tmp = this.snake.slice(-1);
                        crecer = true;
                    }
                    break;
                case 2:
                    if(this.snake[0][0]+1==manzana.posicion[0]&& this.snake[0][1]==manzana.posicion[1]){
                        tmp = this.snake.slice(-1);
                        crecer = true;
                    }
                    break;
                case 3:
                    if(this.snake[0][0]==manzana.posicion[0]&& this.snake[0][1]-1==manzana.posicion[1]){
                        tmp = this.snake.slice(-1);
                        crecer = true;
                    }
                    break;
            }
            for(let i =this.snake.length-1; i>0 ; i--){                
                this.snake[i] = this.snake[i-1];
            }
            switch(this.direccion){
                case 0:
                    console.log(this.snake)
                    this.snake[0] = [this.snake[0][0]-1,this.snake[0][1]];
                    break;
                case 1:
                    this.snake[0] = [this.snake[0][0],this.snake[0][1]+1];
                    break;
                case 2:
                    this.snake[0] = [this.snake[0][0]+1,this.snake[0][1]];
                    break;
                case 3:
                    this.snake[0] = [this.snake[0][0],this.snake[0][1]-1];
                    break;
            }
            if(crecer){
                this.snake.push(tmp)
                return 2;
            }
            return 1;
        }
        return 0;
    } 
}
class Manzana{
    constructor(filas, cols, posiciones_ocupadas){
        let posiciones = [];
        for(let f=0; f<filas;f++){
            for(let c=0; c < cols; c++){
                posiciones.push([f,c]);
            }
        }
        posiciones_ocupadas.forEach(posicion =>{
            posiciones.splice(posiciones.indexOf(posicion),1);
        });
        let el_elegido = Math.round(Math.random()*posiciones.length);
        this.posicion = posiciones[el_elegido];
    }
}
class Score{
    constructor(score_counter_id){
        this.score =0;
        this.score_counter = score_counter_id;
    }
    sumar(){
        this.score += 1;
    }
    show(){
        $("#"+this.score_counter).html(this.score);
    }
}
var snake;
$(document).ready(function(){
    let cols = 10;
    let filas = 10;
    var snake = new Snake(filas,cols);
    var manzanita = new Manzana(filas, cols,snake.snake);
    var score = new Score("score");
    score.show();
    document.addEventListener('keydown', function(event) {
        switch(event.keyCode){
            case 37:
                snake.izquierda();
                break;
            case 38:
                snake.arriba();
                break;
            case 39:
                snake.derecha();
                break;
            case 40:
                snake.abajo();
                break;
        }
    });

    var frames = setInterval(function(){
        dibujar("tabla",filas, cols, snake.snake,manzanita);
    },100);
    var movimiento = setInterval(function(){
        switch(snake.avanzar(manzanita)){
            case 0:
                game_over(frames, movimiento, "game_over", score);
                break;
            case 2:
                manzanita = new Manzana(filas, cols,snake.snake);
                score.sumar();
                score.show();
                break;
        }
    }, 500);
});

function dibujar(div, filas, cols, snake, manzanita){
    let tabla = "<table class='tablero'>";
    for(let f=0; f<filas; f++){
        tabla += "<tr>";
        for(let c=0; c< cols; c++){
            let existe = false;
            snake.forEach(posicion =>{
                if(posicion[0] == f && posicion[1] == c){
                    existe = true;
                }
            });
            if(existe){
                tabla += "<td class='celda snake'></td>";
            }else if(manzanita.posicion[0]==f&&manzanita.posicion[1]==c){
                tabla += "<td class='celda manzanita'></td>";
            }else{
                tabla += "<td class='celda espacio'></td>";
            }
        }
        tabla += "</tr>";
    }
    tabla += "</table>";
    $("#"+div).html(tabla);
}
function game_over(frames, movimiento, div_over, score){
    clearInterval(frames);
    clearInterval(movimiento);
    let over_msg = "<h1 id='game-over'>Game over</h1></br><span id='puntaje_final'>Tu puntaje fue <span id='puntaje_final_num'>"+score.score+"</span></span>";
    $("#"+div_over).html(over_msg);
}